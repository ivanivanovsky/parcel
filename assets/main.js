$(document).ready(function(){
    window.storage = {};
    window.storage.cities = {}
    initNavTabs();
    initRiot();

})

function initNavTabs(){
    var first_section  =$('#main').attr('class');
    $('section').hide()
    $('#'+first_section).show()
    $('#main').attr('class',first_section);
    $('.nav-item').on('click',function(){
        $('.nav-item.active').toggleClass('active');
        $(this).toggleClass('active');
        var sect =  $(this).attr('data-section');
        $('section').hide()
        $('#'+sect).show()
        $('#main').attr('class',sect);
    })

}

function initRiot(){
    riot.mount('*');
}



function cityLoadAutocomplete(cityText){
    var z = $.ajax({
        url : "http://api.cdek.ru/city/getListByTerm/jsonp.php?callback=?",
        dataType : "jsonp",
        data : {
            q : cityText,
            name_startsWith : cityText
        },
        success : function(data) {
            window.storage.cities = data;
            console.log("actual",data)
            $('app-calc')[0]._tag.onCityUpdate();
        }
    });
}

function sendCalcParams(params){
    /*
    * validation block
    *
    * */
    if(params.from != undefined & params.to !=undefined){
        var toServer = {
            senderCity: params.from,
            receiverCity: params.to,
            height: parseInt(params.height),
            width:parseInt( params.width),
            length: parseInt(params.length),
            weight: parseFloat(params.weight),
            cod: parseInt(params.cod),
            price: parseInt(params.price),
            mode: params.mode
        }
        $.ajax({
            method: "POST",
            url: "./api/calculator.php",
            data: toServer,
            success: function(data){
                window.storage.calcResult = JSON.parse(data);
            }
        })
    }

}

var deliverySort = {
    fast :function(a,b){
        if (a.speed < b.speed){
            return -1
        }
        if (a.speed == b.speed){
            return 0
        }
        if (a.speed > b.speed){
            return 1
        }
    },
    price :function(a,b){
        if (a.price < b.price){
            return -1
        }
        if (a.price == b.price){
            return 0
        }
        if (a.price > b.price){
            return 1
        }
    },
    optim : function(a,b){
        if(a.speed < b.speed && a.price < b.price  ){
            return -1
        }
        if(a.speed > b.speed && a.price < b.price  ){
            return -1
        }
        if(a.speed == b.speed && a.price < b.price  ){
            return -1
        }
        if(a.speed == b.speed && a.price == b.price  ){
            return 0
        }
        if(a.speed > b.speed && a.price > b.price  ){
            return 1
        }
        if(a.speed < b.speed && a.price > b.price  ){
            return -1
        }
        return 1
    }
}
